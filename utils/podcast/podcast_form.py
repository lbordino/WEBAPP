import re
from wtforms import SubmitField, StringField, validators, SelectField, TextAreaField, FileField, HiddenField
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired


class PodcastForm(FlaskForm):
    method = HiddenField(
        'Method', [validators.InputRequired()], default='POST')
    id = HiddenField('ID')
    title = StringField('Title', [validators.InputRequired()])
    description = TextAreaField(
        'Description', [validators.InputRequired(), validators.length(max=500)])
    img = FileField('Image', [FileRequired(), FileAllowed(
        ['jpg', 'jpeg', 'png', 'svg'], 'Images only!')])
    category = SelectField('Category', choices=[('Music', 'Music'), ('Comedy', 'Comedy'), ('News', 'News'), ('Sports', 'Sports'), ('Education', 'Education'), ('Business', 'Business'), ('Technology', 'Technology'), ('Arts', 'Arts'), ('Society & Culture', 'Society & Culture'), (
        'Science & Medicine', 'Science & Medicine'), ('Religion & Spirituality', 'Religion & Spirituality'), ('Health', 'Health'), ('Government & Organizations', 'Government & Organizations'), ('Games & Hobbies', 'Games & Hobbies'), ('Kids & Family', 'Kids & Family'), ('TV & Film', 'TV & Film')])
    submit = SubmitField('Submit')
