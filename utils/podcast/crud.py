import os
from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required

import db.db as db
from utils.episode.episode_form import EpisodeForm
from utils.podcast.podcast_form import PodcastForm

podcast = Blueprint('podcast', __name__)

# get all podcasts from the database
@podcast.route('/podcast', methods=['GET'])
def get_all_podcasts():
    form = PodcastForm()
    podcasts = db.get_all_podcasts()
    data = []
    for pod in podcasts:
        data.append(dict(pod))
        data[-1]['CREATOR'] = db.get_user_by_id(data[-1]['CREATOR'])

    return render_template('podcast.html', data=data, form=form)


# add a new podcast to the database
@login_required
@podcast.route('/podcast', methods=['POST'])
def add_podcast():
    form = PodcastForm()
    if form.validate_on_submit():
        img = form.img.data
        id = dict(db.get_max_id())
        if id['MAX(_id)'] == None:
            id = 0
        else:
            id = id['MAX(_id)'] + 1
        new_podcast = {
                "title": form.title.data,
                "description": form.description.data,
                "category": form.category.data,
                "image": f'static/{id}_{img.filename}',
                "creator": current_user.id,
        }
        if(form.method.data == 'POST'  and db.get_podcast(form.id.data) == None):
            success = db.add_podcast(new_podcast)
        elif(form.method.data == 'PUT'):
            tbu = db.get_podcast(form.id.data)
            if (tbu['CREATOR'] != current_user.id):
                flash('Error while creating a podcast', "danger")
                return redirect(url_for('podcast.get_all_podcasts'))
            os.remove(tbu['IMG'])
            success = db.edit_podcast(form.id.data, new_podcast)
        img.save(f'static/{id}_{img.filename}')
    else:
        flash('Error while creating a podcast', "danger")
        return redirect(url_for('podcast.get_all_podcasts'))

    if success:
        return redirect(url_for('podcast.get_all_podcasts'))

    else:
        flash(f"There was an error while handling your request", "danger")


# delete a podcast from the database
@login_required
@podcast.route('/podcast/<id>/delete')
def delete_podcast(id):
    if (db.get_podcast(id)['CREATOR'] != current_user.id):
      flash(f"There was an error while handling your request", "danger")
    os.remove(db.get_podcast(id)['IMG'])
    # remove all episodes
    episodes = db.get_all_episodes(id)
    for ep in episodes:
        ep = dict(ep)
        os.remove(ep['AUDIO'])
        db.remove_episode(ep['ID'])
    success = db.remove_podcast(id)
    if success:
        return redirect(url_for('podcast.get_all_podcasts'))
    else:
        flash(f"There was an error while handling your request", "danger")



# details of a podcast
@podcast.route('/podcast/<id>')
def details(id):
    form = EpisodeForm()
    podcast = db.get_podcast(id)
    data = db.get_all_episodes(id)
    episodes = []
    for pod in data:
        episodes.append(dict(pod))
    podcast = dict(podcast)
    creator = dict(db.get_user_by_id(podcast['CREATOR']))

    return render_template('podcast_details.html', podcast=podcast, creator=creator, episodes=episodes, form=form)

#  handle follow/unfollow podcast
# make an episode favorite or not

@login_required
@podcast.route('/podcast/<id>/follow')
def change_fol(id):
    is_follow = db.is_follow(id)
    if is_follow:
        success = db.change_follow(id, 0)
    else:
        success = db.change_follow(id, 1)

    if success:
        return redirect(url_for('podcast.get_all_podcasts'))
    else:
       flash(f"There was an error while handling your request", "danger")
