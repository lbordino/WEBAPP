import os
from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash
import db.db as db
from utils.episode.episode_form import EpisodeForm

episodes = Blueprint('episodes', __name__)


# add a new episode to the database
@login_required
@episodes.route('/podcast/<id>', methods=['POST'])
def add_episode(id):
    form = EpisodeForm()
    if form.validate_on_submit():
        audio = form.audio.data
        id_ep = dict(db.get_max_id_ep())
        if id_ep['MAX(ID)'] == None:
            id_ep = 0
        else:
            id_ep = id_ep['MAX(ID)'] + 1
        new_episode = {
            "podcast": id,
            "title": form.title.data,
            "audio": f'static/{id}_{id_ep}_{audio.filename}',
            "date": form.date.data,
            "description":  form.description.data,
        }
        success = db.add_episode(new_episode)
        audio.save(f'static/{id}_{id_ep}_{audio.filename}')
        if success:
            return redirect(url_for('podcast.details', id=id))
        else:
            flash(f"Error while creating an episode")
            return redirect(url_for('podcast.details', id=id))
    else:
        errors = [{'field': key, 'messages': form.errors[key]}
                      for key in form.errors.keys()] if form.errors else []
        flash(f"Error while creating an episode: {errors[0]['messages'][0]}", "danger")
        return redirect(url_for('podcast.details', id=id))

# delete an episode from the database
@login_required
@episodes.route('/podcast/<id>/<id_ep>/delete')
def delete_episode(id, id_ep):
    episode = dict(db.get_episode(id, id_ep))
    podcast = dict(db.get_podcast(id))
    if (podcast['CREATOR'] != current_user.id):
        flash(f"There was an error while handling your request", "danger")
    else:
        os.remove(episode['AUDIO'])
        success = db.remove_episode(id_ep)
        if success:
            return redirect(url_for('podcast.details', id=id))
        else:
            flash(f"There was an error while handling your request", "danger")


# make an episode favorite or not
@login_required
@episodes.route('/podcast/<id>/<id_ep>/fav')
def change_fav(id, id_ep):
    is_favorite = db.is_fav(id, id_ep)
    if is_favorite:
        success = db.change_fav(id, id_ep, 0)
    else:
        success = db.change_fav(id, id_ep, 1)

    if success:
        return redirect(url_for('podcast.details', id=id))
    else:
        flash(f"There was an error while handling your request", "danger")

# episode details


@login_required
@episodes.route('/podcast/<id>/<id_ep>')
def details(id, id_ep):
    podcast = dict(db.get_podcast(id))
    ep = dict(db.get_episode(id, id_ep))
    comments = []
    data = db.get_all_comments(id, id_ep)
    for i in data:
        comments.append(dict(i))

    return render_template('episode.html', ep=ep, podcast=podcast, comments=comments)

# add  a comment  on an episode


@login_required
@episodes.route('/podcast/<id>/<id_ep>', methods=['POST'])
def add_comment(id, id_ep):
    if request.form.get('comment') == "":
        flash('Error while creating a comment', "danger")
    new_comment = {
        "podcast": id,
        "episode": id_ep,
        "text": request.form.get('comment'),
    }
    success = db.add_comment(new_comment)
    if success:
        return redirect(url_for('episodes.details', id=id, id_ep=id_ep))
    else:
        flash(f"There was an error while handling your request", "danger")


# delete a comment on an episode
@login_required
@episodes.route('/podcast/<id>/<id_ep>/<id_c>/delete')
def delete_comment(id, id_ep, id_c):
    success = db.remove_comment(id_c)
    if success:
        return redirect(url_for('episodes.details', id=id, id_ep=id_ep))
    else:
       flash(f"There was an error while handling your request", "danger")


# update a comment on an episode
@login_required
@episodes.route('/podcast/<id>/<id_ep>/<id_c>', methods=['POST'])
def update_comment(id, id_ep, id_c):
    if request.form.get('text') == "":
        flash('Error while creating a comment', "danger")
    comment = {
        "id": id_c,
        "text": request.form.get('text'),
    }
    success = db.edit_comment(comment)
    if success:
        return redirect(url_for('episodes.details', id=id, id_ep=id_ep))
    else:
       flash(f"There was an error while handling your request", "danger")
