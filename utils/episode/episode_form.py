import re
from wtforms import SubmitField, StringField, validators, DateField, TextAreaField, FileField,HiddenField
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired


class EpisodeForm(FlaskForm):
    title = StringField('Title', [validators.InputRequired()])
    audio = FileField('Audio', [FileRequired(), FileAllowed(['mp3'], 'Audio only!')])
    date = DateField('Date', [validators.InputRequired()])
    description = TextAreaField('Description', [validators.InputRequired(), validators.length(max=500)])
    submit = SubmitField('Submit')