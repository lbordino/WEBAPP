from wtforms import SubmitField, StringField, EmailField, PasswordField, validators, SelectField
from flask_wtf import FlaskForm


class SignupForm(FlaskForm):
  name = StringField('Name', 
                 [validators.InputRequired()])
  type = SelectField('Type', choices=[('0', 'Listener'), ('1', 'Creator')])
  email = EmailField('Email Address', [validators.InputRequired(), 
             validators.Email(), validators.Length(min=6, max=35)])
  password = PasswordField('New Password', [
        validators.length(min=8),
        validators.InputRequired(),
        validators.EqualTo('confirm', 
                           message='Passwords must match')
        ])
  confirm = PasswordField('Repeat Password', [validators.InputRequired()], id="confirm")
  submit = SubmitField('Submit')