from wtforms import SubmitField, StringField, EmailField, PasswordField, validators, SelectField
from flask_wtf import FlaskForm


class SigninForm(FlaskForm):
  email = EmailField('Email Address', [validators.InputRequired(), 
             validators.Email()])
  password = PasswordField('New Password', [
        validators.InputRequired(),
        ])
  submit = SubmitField('Submit')