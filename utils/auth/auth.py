# import libraries
from flask import Flask, flash, redirect, render_template, request, url_for, session, Blueprint
from flask_session import Session
from flask_login import LoginManager, login_required, login_user, logout_user, current_user
from werkzeug.security import check_password_hash, generate_password_hash

# import local files
from db.db import add_user, get_user_by_email
from model.users import User
from utils.auth.signin_form import SigninForm
from utils.auth.signup_form import SignupForm

auth = Blueprint('auth', __name__, "../static")


@auth.route('/signin', methods=['GET', 'POST'])
def signin():
    form = SigninForm()
    if request.method == 'GET':
        return render_template('signin.html', form=form)
    elif request.method == 'POST':
        if form.validate_on_submit():
            email = form.email.data
            password = form.password.data
            user = get_user_by_email(email)
            if not user or not check_password_hash(user['password'], password):
                flash('Username and password are not valid', 'danger')
                return redirect(url_for('auth.signin'))
            else:
                new = User(user['id'], user['name'], user['email'],
                        user['password'], user['type'])
                login_user(new, True)
                return redirect(url_for('podcast.get_all_podcasts'))
        else:
            flash('Username and password are not valid', 'danger')
            return redirect(url_for('auth.signin'))

@auth.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if request.method == 'GET':
        return render_template('signup.html', form=form)
    elif request.method == 'POST':
        is_valid = form.validate_on_submit()
        if not is_valid or get_user_by_email(form.email.data) != None:
            errors = [{'field': key, 'messages': form.errors[key]}
                      for key in form.errors.keys()] if form.errors else []
            flash(
                f"Error while creating the user: {errors[0]['messages'][0]}", 'danger')
            return render_template('signup.html', form=form, errors=errors)
        new_user = {
            "email": form.email.data,
            "password": generate_password_hash(form.password.data, method='sha256'),
            "name": form.name.data,
            "type": form.type.data
        }
        success = add_user(new_user)

        if success:
            flash('User created', 'success')
            return redirect(url_for('podcast.get_all_podcasts'))
        else:
            flash('Error while creating the user', 'danger')

        return redirect(url_for('auth.signup'))


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))
