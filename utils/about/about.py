import os
from flask import Blueprint, redirect, render_template, request, url_for
from flask_login import current_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash
import db.db as db

about = Blueprint('about', __name__)

# render the about page


@about.route('/about')
def about_page():
    user = dict(db.get_user_by_email(current_user.email))
    followed_podcasts = db.get_all_follow()
    fav_episodes = db.get_all_fav()
    data = []
    fav = []
    for podcast in followed_podcasts:
        data.append(dict(podcast))
    for episode in fav_episodes:
        fav.append(dict(episode))
    return render_template('about.html', user=user, data=data, fav=fav)
