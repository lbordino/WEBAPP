function deletePodcast(id) {
    fetch('/podcast/' + id + '/delete', {
    }).done(location.reload(true))
}

function deleteEpisode(id, id_ep) {
    fetch('/podcast/' + id + '/' + id_ep + '/delete', {
    }).done(location.reload(true))
}

function fav(id, id_ep) {
    fetch('/podcast/' + id + '/' + id_ep + '/fav').then(window.location.reload(true))
}

function deleteComment(id, id_ep, id_c) {
    fetch('/podcast/' + id + '/' + id_ep + '/' + id_c + '/delete', {
    }).then(window.location.reload())

}

window.setTimeout(function () {
    $(".alert").fadeTo(500, 0)
}, 4000);


// a function to search for a substring in the title or description of the podcast
function search() {
    var substring = document.getElementById("search").value;
    var podcasts = document.getElementsByClassName("ep_card");
    for (var i = 0; i < podcasts.length; i++) {
        var title = podcasts[i].getElementsByClassName("card-title")[0].innerHTML;
        var description = podcasts[i].getElementsByClassName("description")[0].innerHTML;
        if (title.includes(substring) || description.includes(substring)) {
            podcasts[i].style.display = "block";
        } else {
            podcasts[i].style.display = "none";
        }
    }
}

document.getElementById("search").addEventListener("keyup", search);