from flask_login import LoginManager, UserMixin
import db.db as db

class User(UserMixin):
    def __init__(self,id, name, email, password, type):
        self.id = id
        self.name = name
        self.email = email
        self.password = password
        self.type = type
        
