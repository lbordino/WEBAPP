import sqlite3

from flask_login import current_user


def get_user_by_email(email):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = 'SELECT * FROM USERS WHERE EMAIL = ?'
    cursor.execute(sql, (email,))
    user = cursor.fetchone()

    cursor.close()
    conn.close()

    return user


def get_user_by_id(id):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = 'SELECT * FROM USERS WHERE id = ?'
    cursor.execute(sql, (id,))
    user = cursor.fetchone()

    cursor.close()
    conn.close()

    return user


def add_user(user):

    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'INSERT INTO USERS(EMAIL,TYPE,NAME,PASSWORD) VALUES(?,?,?,?)'

    try:
        cursor.execute(
            sql, (user['email'], user['type'], user['name'], user['password']))
        conn.commit()
        success = True
    except Exception as e:
        # TODO: log error
        print('ERROR', str(e))
        conn.rollback()

    cursor.close()
    conn.close()

    return success


# add a podcast to the database
def add_podcast(podcast):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'INSERT INTO PODCASTS(TITLE,DESCRIPTION,CATEGORY,IMG,CREATOR) VALUES(?,?,?,?,?)'

    try:
        cursor.execute(
            sql, (podcast['title'], podcast['description'], podcast['category'], podcast['image'], podcast['creator']))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success

# remove a podcast from the database


def remove_podcast(podcast_id):

    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'DELETE FROM PODCASTS WHERE _id = ?'

    try:
        cursor.execute(sql, (podcast_id,))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success

# get all podcasts from the database


def get_all_podcasts():
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()
    if current_user.is_authenticated:
        sql = '''
                SELECT *
                FROM PODCASTS P
                LEFT OUTER JOIN FOLLOW F
                ON P._id = F.PODCAST AND F.USER = ?
            '''
        cursor.execute(sql, (current_user.id,))
    else:
        sql = 'SELECT * FROM PODCASTS'
        cursor.execute(sql)
    podcasts = cursor.fetchall()

    cursor.close()
    conn.close()

    return podcasts


# get a podcast from the database
def get_podcast(podcast_id):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()
    sql = '''
        SELECT * 
        FROM PODCASTS 
        JOIN USERS
        ON PODCASTS.CREATOR = USERS.ID
        WHERE _id = ?
        '''
    cursor.execute(sql, (podcast_id,))
    podcast = cursor.fetchone()
    cursor.close()
    conn.close()

    return podcast

# edit a podcast from the database


def edit_podcast(podcast_id, podcast):

    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'UPDATE PODCASTS SET TITLE = ?, DESCRIPTION = ?, CATEGORY = ?, IMG = ?, CREATOR = ? WHERE _id = ?'

    try:
        cursor.execute(
            sql, (podcast['title'], podcast['description'], podcast['category'], podcast['image'], podcast['creator'], podcast_id))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success

# remove a podcast from the database


def remove_podcast(podcast_id):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'DELETE FROM PODCASTS WHERE _id = ?'

    try:
        cursor.execute(sql, (podcast_id,))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success

# get all episodes of series from the database


def get_all_episodes(series_id):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()
    if current_user.is_authenticated:
        sql = '''
                SELECT * 
                FROM EPISODES E
                LEFT OUTER JOIN FAVOURITE F
                ON E.ID = F.EPISODE AND F.PODCAST = E.PODCAST AND F.USER = ?
                WHERE E.PODCAST = ?
                ORDER BY E.DATE DESC
                '''
        cursor.execute(sql, (current_user.id, series_id,))
    else:
        sql = 'SELECT * FROM EPISODES WHERE PODCAST = ? ORDER BY DATE DESC'
        cursor.execute(sql, (series_id,))
    episodes = cursor.fetchall()

    cursor.close()
    conn.close()

    return episodes


# add a episode to the database
def add_episode(episode):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'INSERT INTO EPISODES(PODCAST,TITLE,AUDIO,DATE,DESCRIPTION) VALUES(?,?,?,?,?)'

    try:
        cursor.execute(
            sql, (episode['podcast'], episode['title'], episode['audio'], episode['date'], episode['description']))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success


# remove a episode from the database
def remove_episode(episode_id):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'DELETE FROM EPISODES WHERE ID = ?'

    try:
        cursor.execute(sql, (episode_id,))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success

# get a episode from the database


def get_episode(id, episode_id):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = 'SELECT * FROM EPISODES WHERE PODCAST = ? AND ID = ?'
    cursor.execute(sql, (id, episode_id,))
    episode = cursor.fetchone()

    cursor.close()
    conn.close()

    return episode


# get max value of id from podcast table
def get_max_id():
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = 'SELECT MAX(_id) FROM PODCASTS'
    cursor.execute(sql)
    max_id = cursor.fetchone()

    cursor.close()
    conn.close()

    return max_id

# get max value of id from episode table


def get_max_id_ep():
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = 'SELECT MAX(ID) FROM EPISODES'
    cursor.execute(sql)
    max_id = cursor.fetchone()
    cursor.close()
    conn.close()
    return max_id

# make a podcast favourite


def change_fav(id, id_ep, status):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    if status == 0:
        sql = 'DELETE FROM FAVOURITE WHERE EPISODE = ? AND USER = ? AND PODCAST = ?'
    elif status == 1:
        sql = 'INSERT INTO FAVOURITE(EPISODE, USER, PODCAST) VALUES(?,?,?)'
    try:
        cursor.execute(sql, (id_ep, current_user.id, id,))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success

# get all favourite episodes of user from the database


def get_all_fav():
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = '''
            SELECT * 
            FROM FAVOURITE F
            LEFT JOIN EPISODES E
            ON F.EPISODE = E.ID
            WHERE F.USER = ?
            '''
    cursor.execute(sql, (current_user.id,))
    episodes = cursor.fetchall()

    cursor.close()
    conn.close()

    return episodes

# is episode favourite


def is_fav(id, id_ep):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = 'SELECT * FROM FAVOURITE WHERE EPISODE = ? AND USER = ? AND PODCAST = ?'
    cursor.execute(sql, (id_ep, current_user.id, id,))
    episode = cursor.fetchone()

    cursor.close()
    conn.close()

    return episode

# get all follow podcast of a user from the database


def get_all_follow():
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = '''
            SELECT * 
            FROM FOLLOW F
            LEFT JOIN PODCASTS P
            ON F.PODCAST = P._id
            WHERE F.USER = ?
            '''
    cursor.execute(sql, (current_user.id,))
    podcasts = cursor.fetchall()

    cursor.close()
    conn.close()

    return podcasts

# is podcast followed


def is_follow(id):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = 'SELECT * FROM FOLLOW WHERE PODCAST = ? AND USER = ?'
    cursor.execute(sql, (id, current_user.id,))
    podcast = cursor.fetchone()

    cursor.close()
    conn.close()

    return podcast

# change follow status


def change_follow(id, status):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    if status == 0:
        sql = 'DELETE FROM FOLLOW WHERE PODCAST = ? AND USER = ?'
    elif status == 1:
        sql = 'INSERT INTO FOLLOW(PODCAST, USER) VALUES(?,?)'
    try:
        cursor.execute(sql, (id, current_user.id,))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success

# get all followed podcast of a user


def get_all_follow():
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = '''
            SELECT * 
            FROM FOLLOW F
            JOIN PODCASTS P
            ON F.PODCAST = P._id
            WHERE F.USER = ?
            '''
    cursor.execute(sql, (current_user.id,))
    podcasts = cursor.fetchall()

    cursor.close()
    conn.close()

    return podcasts

# get all comments of a episode from the database


def get_all_comments(id, id_ep):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    sql = '''
            SELECT *
            FROM COMMENTS C
            JOIN USERS U
            ON C.USER = U.id   
            WHERE C.EPISODE = ? AND C.PODCAST = ?
          '''
    cursor.execute(sql, (id_ep, id,))
    comments = cursor.fetchall()

    cursor.close()
    conn.close()

    return comments

# add a comment to the database


def add_comment(comment):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'INSERT INTO COMMENTS(PODCAST, EPISODE, USER, TEXT) VALUES(?,?,?,?)'
    try:
        cursor.execute(
            sql, (comment['podcast'], comment['episode'], current_user.id, comment['text'],))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success


# remove a comment from the database
def remove_comment(id):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'DELETE FROM COMMENTS WHERE ID = ? AND USER = ? '
    try:
        cursor.execute(sql, (id, current_user.id,))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success


def edit_comment(comment):
    conn = sqlite3.connect('db/db.db')
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    success = False
    sql = 'UPDATE COMMENTS SET TEXT = ? WHERE USER = ? AND ID = ?'
    try:
        cursor.execute(
            sql, (comment['text'], current_user.id, comment['id'],))
        conn.commit()
        success = True
    except Exception as e:
        print('ERROR', str(e))
        conn.rollback()

    return success
