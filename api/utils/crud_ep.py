import os
from flask import Blueprint, redirect, render_template, request, url_for
from flask_login import current_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash
import db.db as db

episodes = Blueprint('episodes', __name__)


# add a new episode to the database
@login_required
@episodes.route('/podcast/<id>', methods=['POST'])
def add_episode(id):
    audio = request.files['audio']
    id_ep = dict(db.get_max_id_ep())
    if id_ep['MAX(ID)'] == None:
        id_ep = 0
    else:
        id_ep = id_ep['MAX(ID)'] + 1
    if audio:
        audio.save(f'static/{id}_{id_ep}_{audio.filename}')
    new_episode = {
        "podcast": id,
        "title": request.form.get('title'),
        "audio": f'static/{id}_{id_ep}_{audio.filename}',
        "date": request.form.get('date'),
        "description": request.form.get('description'),
    }
    success = db.add_episode(new_episode)
    if success:
        return redirect(url_for('podcast.details', id=id))
    else:
        return "Error"


# delete an episode from the database
@login_required
@episodes.route('/podcast/<id>/<id_ep>/delete')
def delete_episode(id, id_ep):
    episode = dict(db.get_episode(id, id_ep))
    podcast = dict(db.get_podcast(id))
    if (podcast['CREATOR'] != current_user.id):
        return "Error"
    else:
        os.remove(episode['AUDIO'])
        success = db.remove_episode(id_ep)
        if success:
            return redirect(url_for('podcast.details', id=id))
        else:
            return "Error"


# make an episode favorite or not
@login_required
@episodes.route('/podcast/<id>/<id_ep>/fav')
def change_fav(id, id_ep):
    is_favorite = db.is_fav(id, id_ep)
    if is_favorite:
        success = db.change_fav(id, id_ep, 0)
    else:
        success = db.change_fav(id, id_ep, 1)

    if success:
        return redirect(url_for('podcast.details', id=id))
    else:
        return "Error"

# episode details


@login_required
@episodes.route('/podcast/<id>/<id_ep>')
def details(id, id_ep):
    podcast = dict(db.get_podcast(id))
    ep = dict(db.get_episode(id, id_ep))
    comments = []
    data = db.get_all_comments(id, id_ep)
    for i in data:
        comments.append(dict(i))

    return render_template('episode.html', ep=ep, podcast=podcast, comments=comments)

# add  a comment  on an episode


@login_required
@episodes.route('/podcast/<id>/<id_ep>', methods=['POST'])
def add_comment(id, id_ep):
    new_comment = {
        "podcast": id,
        "episode": id_ep,
        "text": request.form.get('comment'),
    }
    success = db.add_comment(new_comment)
    if success:
        return redirect(url_for('episodes.details', id=id, id_ep=id_ep))
    else:
        return "Error"


# delete a comment on an episode
@login_required
@episodes.route('/podcast/<id>/<id_ep>/<id_c>/delete')
def delete_comment(id, id_ep, id_c):
    success = db.remove_comment(id_c)
    if success:
        return redirect(url_for('episodes.details', id=id, id_ep=id_ep))
    else:
        return "Error"


# update a comment on an episode
@login_required
@episodes.route('/podcast/<id>/<id_ep>/<id_c>', methods=['POST'])
def update_comment(id, id_ep, id_c):
    comment = {
        "id": id_c,
        "text": request.form.get('text'),
    }
    success = db.edit_comment(comment)
    if success:
        return redirect(url_for('episodes.details', id=id, id_ep=id_ep))
    else:
        return "Error"
