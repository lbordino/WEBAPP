import os
from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required

import db.db as db

podcast = Blueprint('podcast', __name__)

# get all podcasts from the database


@podcast.route('/podcast', methods=['GET'])
def get_all_podcasts():
    podcasts = db.get_all_podcasts()
    data = []
    for pod in podcasts:
        data.append(dict(pod))
        data[-1]['CREATOR'] = db.get_user_by_id(data[-1]['CREATOR'])

    return render_template('podcast.html', data=data)


# add a new podcast to the database
@login_required
@podcast.route('/podcast', methods=['POST'])
def add_podcast():
    if request.form.get('_method') == 'PUT':
        return edit_podcast(request.form.get('_id'))
    img = request.files['img']
    if img:
        id = dict(db.get_max_id())
        if id['MAX(_id)'] == None:
            id = 0
        else:
            id = id['MAX(_id)'] + 1
        new_podcast = {
            "title": request.form.get('title'),
            "description": request.form.get('description'),
            "category": request.form.get('category'),
            "image": f'static/{id}_{img.filename}',
            "creator": current_user.id,
        }
        img.save(f'static/{id}_{img.filename}')
        success = db.add_podcast(new_podcast)
    else:
        flash('Please upload an image', "danger")
        return redirect(url_for('podcast.get_all_podcasts'))

    if success:
        return redirect(url_for('podcast.get_all_podcasts'))

    else:
        return "Error"

# delete a podcast from the database


@login_required
@podcast.route('/podcast/<id>/delete')
def delete_podcast(id):
    if (db.get_podcast(id)['CREATOR'] != current_user.id):
        # TODO: redirect to error page with 403 error
        return "Error"
    os.remove(db.get_podcast(id)['IMG'])
    # remove all episodes
    episodes = db.get_all_episodes(id)
    for ep in episodes:
        ep = dict(ep)
        os.remove(ep['AUDIO'])
        db.remove_episode(ep['ID'])
    success = db.remove_podcast(id)
    if success:
        return redirect(url_for('podcast.get_all_podcasts'))
    else:
        return "Error"

# edit a podcast from the database


@login_required
@podcast.route('/podcast/<id>', methods=['PUT'])
def edit_podcast(id):
    pod = dict(db.get_podcast(id))
    img = request.files['img']
    if img == None:
        img = pod['IMG']
    if img:
        if pod['IMG'] != '':
            os.remove(pod['IMG'])
        img.save(f'static/{id}_{img.filename}')
    edited_podcast = {
        "title": request.form.get('title'),
        "description": request.form.get('description'),
        "category": request.form.get('category'),
        "image": f'static/{id}_{img.filename}',
        "creator": current_user.id,
    }
    if (pod['CREATOR'] != current_user.id):
        # TODO: redirect to error page with 403 error
        return "Error"
    success = db.edit_podcast(id, edited_podcast)
    if success:
        return redirect(url_for('podcast.get_all_podcasts'))
    else:
        return "Error"


# details of a podcast
@podcast.route('/podcast/<id>')
def details(id):
    podcast = db.get_podcast(id)
    data = db.get_all_episodes(id)
    episodes = []
    for pod in data:
        episodes.append(dict(pod))
    podcast = dict(podcast)
    creator = dict(db.get_user_by_id(podcast['CREATOR']))

    return render_template('podcast_details.html', podcast=podcast, creator=creator, episodes=episodes)

#  handle follow/unfollow podcast
# make an episode favorite or not


@login_required
@podcast.route('/podcast/<id>/follow')
def change_fol(id):
    is_follow = db.is_follow(id)
    if is_follow:
        success = db.change_follow(id, 0)
    else:
        success = db.change_follow(id, 1)

    if success:
        return redirect(url_for('podcast.get_all_podcasts'))
    else:
        return "Error"
