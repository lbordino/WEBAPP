# import libraries
from flask import Flask, flash, redirect, render_template, request, url_for, session, Blueprint
from flask_session import Session
from flask_login import LoginManager, login_required, login_user, logout_user, current_user
from werkzeug.security import check_password_hash, generate_password_hash

# import local files
from db.db import add_user, get_user_by_email
from model.users import User

auth = Blueprint('auth', __name__, "../static")


@auth.route('/signin')
def signin():
    return render_template('signin.html')


@auth.route('/signin', methods=['POST'])
def signin_post():
    email = request.form.get('email')
    password = request.form.get('password')

    user = get_user_by_email(email)

    if not user or not check_password_hash(user['password'], password):
        flash('Username and password are not valid', 'danger')
        return redirect(url_for('auth.signin'))
    else:
        new = User(user['id'], user['name'], user['email'], user['password'], user['type'])
        login_user(new, True)
        return redirect(url_for('podcast.get_all_podcasts'))


@auth.route('/signup')
def signup():
    return render_template('signup.html')


@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    password = request.form.get('password')

    user_in_db = get_user_by_email(email)

    if user_in_db:
        flash('Error credential are not valid', 'danger')
        return redirect(url_for('auth.signup'))
    else:
        new_user = {
            "email": email,
            "password": generate_password_hash(password, method='sha256'),
            "name": request.form.get('name'),
            "type": request.form.get('type')
        }

        success = add_user(new_user)

        if success:
            flash('User created', 'success')
            return redirect(url_for('podcast.get_all_podcasts'))
        else:
            flash('Error while creating the user', 'danger')

    return redirect(url_for('auth.signup'))


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))
