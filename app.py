# import libraries
from flask import Flask, flash, redirect, render_template, request, url_for, session, Blueprint
from flask_session import Session
from flask_login import LoginManager, login_required, login_user, logout_user, current_user
from werkzeug.security import check_password_hash, generate_password_hash
from wtforms import SubmitField, BooleanField, StringField, PasswordField, validators, SelectField
from flask_wtf import Form

# import local files
import db.db as db
from model.users import User
from utils.auth.auth import auth
from utils.podcast.crud import podcast
from utils.episode.crud import episodes
from utils.about.about import about


# create the application
app = Flask(__name__)
app.secret_key = '89cdncdiicJCNCBncsBCSDBCjs'
app.config['SESSION_TYPE'] = 'filesystem'
app.config['SESSION_PERMANENT'] = False
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
Session(app)

# blueprint for non-auth parts of app
app.register_blueprint(auth)
app.register_blueprint(podcast)
app.register_blueprint(episodes)
app.register_blueprint(about)

# login set-up
login_manager = LoginManager()
login_manager.login_view = 'auth.signin'
login_manager.login_message = 'Login to access this page'
login_manager.login_message_category = 'warning'
login_manager.init_app(app)


@login_manager.user_loader
def load_user(id):
    utente = db.get_user_by_id(id)

    if utente is not None:
        user = User(utente['id'], utente['name'],
                    utente['email'], utente['password'], utente['type'])
    else:
        user = None

    return user


@app.route('/')
def home():
    return render_template('home.html')


# flask run debug on port 5555
if __name__ == '__main__':
    app.run(debug=True, port=5550)
