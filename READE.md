# Al fine di accendere il webserver eseguire:

```
pip3 -r install requirements.txt
flask run

```
----
## Configurazione 
### Utenti di test

- admin@admin.com:admin2023
- listener@listener.com:listener2023

### Immagini
le immagini si trovano nella cartella config/img_test.

**Formati supportati:**
- jpg
- jpeg 
- png
- gif
### Audio
gli audio si trovano nella cartella config/audio_test
formato supportato **.mp3**